import javax.rmi.CORBA.Util;
import java.util.ArrayList;
import java.util.Map;

public class Handler {
    public static void addMenu(ArrayList<Menu> menuList) {
        menuList.add(new Menu("Nasi Goreng", 15000));
        menuList.add(new Menu("Mie Goreng", 13000));
        menuList.add(new Menu("Nasi + Ayam", 18000));
        menuList.add(new Menu("Es Teh", 3000));
        menuList.add(new Menu("Es Jeruk", 5000));
    }

    public static void showMenu(ArrayList<Menu> menuList) {
        Utils.header("Selamat datang di BinarFud");
        System.out.println("Silahkan pilih makanan:");

        int index = 1;
        for (Menu menu : menuList) {
            System.out.println(index + ". " + menu.getName() + "\t| " + Utils.formatRupiah(menu.getPrice()));
            index++;
        }

        System.out.println("99. Pesan dan Bayar");
        System.out.println("0. Keluar aplikasi");
        System.out.println();
    }

    public static void confirmMenu(Menu menu) {
        Utils.header("Berapa pesanan anda");

        System.out.println(menu.getName() + "\t| " + Utils.formatRupiah(menu.getPrice()));
        System.out.println("(Input 0 untuk kembali)");
        System.out.println();
    }

    public static void confirmPayment(Map<String, Order> orderList) {
        Utils.header("Konfirmasi & Pembayaran");

        int totalQuantity = 0, totalPrice = 0;
        for (String foodName : orderList.keySet()) {
            int quantity = orderList.get(foodName).getQuantity();
            int price = orderList.get(foodName).getFood().getPrice();
            int finalPrice = quantity * price;

            totalQuantity += quantity;
            totalPrice += finalPrice;
            System.out.println(foodName + "\t " + quantity + "\t" + Utils.formatRupiah(finalPrice));
        }

        System.out.println("--------------------------------+");

        System.out.println("Total" + "\t\t" + totalQuantity + "\t" + Utils.formatRupiah(totalPrice));
        System.out.println();

        System.out.println("1. Konfirmasi dan bayar");
        System.out.println("2. Kembali ke menu utama");
        System.out.println("0. Keluar aplikasi");
        System.out.println();
    }

}
