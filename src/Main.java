import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {
    static ArrayList<Menu> menuList = new ArrayList<Menu>();
    static Map<String, Order> orderList = new HashMap<String, Order>();

    public static void main(String[] args) throws Exception {
        Scanner input = new Scanner(System.in);
        /* ======== initialize menu ======== */
        Handler.addMenu(menuList);

        while (true) {
            /* ======== show menu ======== */
            Handler.showMenu(menuList);
            System.out.print("=> ");
            int operation = input.nextInt();
            System.out.println();

            /* ======== handle user exit ======== */
            if (operation == 0) {
                Utils.userExit();
                break;
            }
            /* ======== handle payment ======== */
            else if (operation == 99) {
                /* ======== show confirmation and payment ======== */
                if (orderList.isEmpty()) {
                    System.out.println("[ERROR] Anda belum memesan apapun!\n");
                    continue;
                }
                Handler.confirmPayment(orderList);

                /* ======== show confirmation choice ======== */
                System.out.print("=> ");
                int confirmation = input.nextInt();
                System.out.println();

                /* ======== handle user exit ======== */
                if (confirmation == 0) {
                    Utils.userExit();
                    break;
                }
                /* ======== handle payment ======== */
                else if (confirmation == 1) {
                    Utils.showInvoice(orderList);
                    Utils.createInvoice(orderList);
                    orderList.clear();
                }
                /* ======== handle user back to main menu ======== */
                else if (confirmation == 2)
                    continue;
                    /* ======== handle invalid input ======== */
                else {
                    System.out.println("[ERROR] Tolong hanya masukan nomor sesuai dengan yang tertera!\n");
                    continue;
                }
            }
            /* ======== handle purchase of item ======== */
            else if (operation >= 1 && operation <= menuList.size()) {
                /* ======== get quantity of menu ======== */
                Menu selectedMenu = menuList.get(operation - 1);
                Handler.confirmMenu(selectedMenu);
                System.out.print("qty => ");
                int quantity = input.nextInt();
                System.out.println();

                /* ======== handle user back to main menu ======== */
                if (quantity == 0)
                    continue;
                    /* ======== handle food quantity in order ======== */
                else if (quantity > 0) {
                    String menuName = selectedMenu.getName();
                    boolean foodExistInMenu = orderList.containsKey(menuName);
                    /* ======== handle same food ======== */
                    if (foodExistInMenu)
                        orderList.put(menuName, new Order(selectedMenu, orderList.get(menuName).getQuantity() + quantity));
                    else
                        orderList.put(menuName, new Order(selectedMenu, quantity));
                    System.out.println("Makanan berhasil ditambahkan ke daftar!\n");
                }
                /* ======== handle invalid input ======== */
                else {
                    System.out.println("[ERROR] Quantity tidak boleh bilangan negatif!\n");
                    continue;
                }
            }
            /* ======== handle invalid input ======== */
            else {
                System.out.println("[ERROR] Tolong hanya masukan nomor sesuai dengan yang tertera!\n");
                continue;
            }
        }
        input.close();
    }

}