public class Order {
    private Menu food;
    private int quantity;

    public Order(Menu food, int quantity) {
        this.food = food;
        this.quantity = quantity;
    }

    public Menu getFood() {
        return this.food;
    }

    public void setFood(Menu food) {
        this.food = food;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
