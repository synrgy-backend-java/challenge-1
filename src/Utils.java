import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Random;

public class Utils {
    public static void header(String header) {
        System.out.println("==========================");
        System.out.println(header);
        System.out.println("==========================");
        System.out.println();
    }
    public static void showInvoice(Map<String, Order> orderList) {
        header("BinarFud");

        System.out.println("Terimakasih sudah memesan di BinarFud");
        System.out.println();

        System.out.println("Dibawah ini adalah pesanan anda");
        System.out.println();

        int totalQuantity = 0, totalPrice = 0;
        for (String foodName : orderList.keySet()) {
            int quantity = orderList.get(foodName).getQuantity();
            int price = orderList.get(foodName).getFood().getPrice();
            int finalPrice = quantity * price;

            totalQuantity += quantity;
            totalPrice += finalPrice;

            System.out.println(foodName + "\t " + quantity + "\t" + formatRupiah(finalPrice));
        }

        System.out.println("--------------------------------+");

        System.out.println("Total" + "\t\t" + totalQuantity + "\t" + formatRupiah(totalPrice));
        System.out.println();

        System.out.println("Pembayaran : BinarCash");
        System.out.println();

        header("Simpan struk ini sebagai\nbukti pembayaran");
    }

    public static void createInvoice(Map<String, Order> orderList) {
        try {
            String atoz = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            String fileName = "invoice_" + generateRandom(atoz) + ".txt";

            FileWriter myWriter = new FileWriter(fileName);

            myWriter.write("==========================\n");
            myWriter.write("BinarFud\n");
            myWriter.write("==========================\n\n");

            myWriter.write("Terimakasih sudah memesan di BinarFud\n\n");

            myWriter.write("Dibawah ini adalah pesanan anda\n\n");

            int totalQuantity = 0, totalPrice = 0;
            for (String foodName : orderList.keySet()) {
                int quantity = orderList.get(foodName).getQuantity();
                int price = orderList.get(foodName).getFood().getPrice();
                int finalPrice = quantity * price;

                totalQuantity += quantity;
                totalPrice += finalPrice;
                myWriter.write(foodName + "\t " + quantity + "\t" + formatRupiah(finalPrice) + "\n");
            }
            myWriter.write("--------------------------------+\n");

            myWriter.write("Total" + "\t\t" + totalQuantity + "\t" + formatRupiah(totalPrice) + "\n\n");

            myWriter.write("Pembayaran : BinarCash\n\n");

            myWriter.write("==========================\n");
            myWriter.write("Simpan struk ini sebagai\n");
            myWriter.write("bukti pembayaran\n");
            myWriter.write("==========================");
            myWriter.close();

            System.out.println("Riwayat telah disimpan di " + fileName);
            System.out.println();
        } catch (IOException e) {
            System.out.println("Terjadi kesalahan.");
            e.printStackTrace();
        }
    }

    public static void userExit() {
        System.out.println("Anda telah keluar dari aplikasi!\n");
    }

    public static String formatRupiah(int price) {
        String strPrice = String.valueOf(price);
        int length = strPrice.length();
        int counter = 0;
        String result = "";
        for (int i = length - 1; i >= 0; i--) {
            result += strPrice.charAt(i);
            counter++;
            if (counter == 3 && i != 0) {
                result += ".";
                counter = 0;
            }
        }
        String finalResult = "Rp. ";
        for (int i = result.length() - 1; i >= 0; i--) {
            finalResult += result.charAt(i);
        }
        return finalResult;
    }

    private static String generateRandom(String aToZ) {
        Random rand=new Random();
        StringBuilder res=new StringBuilder();
        for (int i = 0; i < 10; i++) {
            int randIndex=rand.nextInt(aToZ.length());
            res.append(aToZ.charAt(randIndex));
        }
        return res.toString();
    }

}
